# Copyright 2016-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=google tag=v${PV} ] cmake
require alternatives

export_exlib_phases src_install

SUMMARY="Efficient binary <-> decimal conversion routines for IEEE doubles"
DESCRIPTION="
This project provides binary-decimal and decimal-binary
routines for IEEE doubles.

The library consists of efficient conversion routines that have been extracted
from the V8 JavaScript engine. The code has been refactored and improved so
that it can be used more easily in other projects.
"

LICENCES="BSD-3"
MYOPTIONS=""

DEPENDENCIES="
    run:
        !dev-libs/double-conversion:0[<3.0.2] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DBUILD_SHARED_LIBS:BOOL=TRUE
)

double-conversion_src_install() {
    local arch_dependent_alternatives=()
    local host=$(exhost --target)

    cmake_src_install

    arch_dependent_alternatives+=(
        /usr/${host}/include/${PN}   ${PN}-${SLOT}
        /usr/${host}/lib/lib${PN}.so ${PN}-${SLOT}.so
        /usr/${host}/lib/cmake/${PN} ${PN}-${SLOT}
    )

    alternatives_for _${host}_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"
}

