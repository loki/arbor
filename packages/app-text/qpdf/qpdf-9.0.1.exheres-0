# Copyright 2012-2019 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ release=release-${PNV} suffix=tar.gz ] \
    bash-completion

SUMMARY="C++ library and set of programs that inspect and manipulate the structure of PDF files"
DESCRIPTION="
QPDF is a C++ library and set of programs that inspect and manipulate the
structure of PDF files. It can encrypt and linearize files, expose the
internals of a PDF file, and do many other operations useful to end users and
PDF developers.
"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}/files/${PN}-manual.html [[ lang = en ]]"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        sys-libs/zlib
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
    test:
        app-text/ghostscript[>=9.21-r4][tiff]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-os-secure-random
    --disable-avoid-windows-handle
    --disable-check-autofiles
    --disable-doc-maintenance
    --disable-html-doc
    --disable-insecure-random
    --disable-int-warnings
    --disable-oss-fuzz
    --disable-pdf-doc
    --disable-static
    --disable-validate-doc
    --disable-werror
    --with-random=/dev/urandom
)

# TODO: Use src_test_expensive to run the largefiles test suite
DEFAULT_SRC_CONFIGURE_TESTS=(
    '--enable-show-failed-test-output --disable-show-failed-test-output'
    '--enable-test-compare-images --disable-test-compare-images'
)

BASH_COMPLETIONS=(
    completions/bash/qpdf
)

src_install() {
    default

    bash-completion_src_install
}

