# Copyright 2013 NAKAMURA Yoshitaka
# Copyright 2015 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MPV=${PV/_rc/rc}
CBOOTSTRAP="${PN}1.4-bootstrap-20171003"

export_exlib_phases pkg_setup src_unpack src_prepare src_compile src_install src_test

SUMMARY="The Go Programming Language"
HOMEPAGE="https://golang.org"
DOWNLOADS="
    https://storage.googleapis.com/golang/${PN}${MPV}.src.tar.gz
    platform:amd64? (
        libc:musl? (
            https://dl.google.com/${PN}/${CBOOTSTRAP}.tar.gz
        )
        !libc:musl? (
            https://storage.googleapis.com/golang/${PN}${MPV/_rc/rc}.linux-amd64.tar.gz
        )
    )
    platform:armv7? ( https://storage.googleapis.com/golang/${PN}${MPV/_rc/rc}.linux-armv6l.tar.gz )
    platform:armv8? ( https://storage.googleapis.com/golang/${PN}${MPV/_rc/rc}.linux-arm64.tar.gz )
"

LICENCES="BSD-3"
SLOT="0"
MYOPTIONS="
    ( libc: musl )
    ( platform: amd64 armv7 armv8 )
"

# strip - restricted because dwarf_compress can cause go to run into strange segfaults
# test  - causes sydbox to hit invalid pointer errors. Lots of tests also require to bind
# or connect to an address (mostly localhost)
RESTRICT="strip test"

DEPENDENCIES=""

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( PATENTS )

_go_arch_name() {
    case $1 in
        aarch64-unknown-linux-gnueabi)
            echo arm64
            ;;
        x86_64-pc-linux-gnu)
            echo amd64
            ;;
        x86_64-pc-linux-musl)
            echo amd64
            ;;
        armv7-unknown-linux-gnueabihf)
            echo arm
            ;;
    esac
}

go_pkg_setup() {
    case "$(exhost --target)" in
        aarch64-unknown-linux-gnueabi)
            export go_platform=linux-arm64
        ;;
        x86_64-pc-linux-gnu)
            export go_platform=linux-amd64
        ;;
        x86_64-pc-linux-musl)
            export go_platform=linux-amd64
        ;;
        armv7-unknown-linux-gnueabihf)
            export go_platform=linux-armv6l
        ;;
        *)
            die "Your target isn't supported by the exheres yet. Please submit a patch adding support."
        ;;
    esac
    local host=$(exhost --build) target=$(exhost --target)
    export go_host_arch=$(_go_arch_name ${host} )
    export go_arch=$(_go_arch_name ${target} )
}

go_src_unpack() {
    unpack "${PN}${MPV}.src.tar.gz"
    edo mv "${WORKBASE}"/${PN} "${WORKBASE}"/${PNV}
    if ! has_version --root "dev-lang/go:0"; then
        if option libc:musl; then
            unpack "${CBOOTSTRAP}.tar.gz"
        else
            unpack "${PN}${MPV}.${go_platform}.tar.gz"
        fi
        edo mv "${WORKBASE}"/${PN} "${WORKBASE}"/${PN}-bootstrap
    fi
}

go_src_prepare() {
    if ! has_version --root "dev-lang/go:0" && option libc:musl; then
        # ar is banned
        edo sed -i "s,\"ar\",\"$(exhost --tool-prefix)ar\"," \
                "${WORKBASE}"/${PN}-bootstrap/src/cmd/dist/build.c
    fi

    default
}

go_src_compile() {
    export GOROOT_FINAL=/usr/$(exhost --target)/lib/go

    if ! has_version --root "dev-lang/go:0"; then
        # Compile the C bootstrap
        if option libc:musl; then
            edo cd "${WORKBASE}"/${PN}-bootstrap/src
            edo env CGO_ENABLED=0 GOHOSTARCH=${go_host_arch} ./make.bash
        fi

        edo cd "${WORKBASE}"/${PNV}/src
        edo env GOARCH=${go_arch} GOHOSTARCH=${go_host_arch} GOROOT_BOOTSTRAP="${WORKBASE}"/${PN}-bootstrap ./make.bash
    else
        edo cd "${WORKBASE}"/${PNV}/src
        if [[ -x "/usr/$(exhost --target)/lib/go/bin/go" ]];then
            edo env GOARCH=${go_arch} GOHOSTARCH=${go_host_arch} GOROOT_BOOTSTRAP=/usr/$(exhost --target)/lib/go ./make.bash
        # this elif statement is needed to fix upgrading straight from 1.4.2.
        # since the GOROOT layout was changed when it was upgraded to 1.5.
        elif [[ -x "/usr/$(exhost --target)/bin/go" ]];then
            edo mkdir "${WORK}"/bootstrap
            edo pushd "${WORK}"/bootstrap
            for dir in /usr/$(exhost --target)/lib/go/{include,lib,pkg,src};do
                edo ln -s "${dir}" ./
            done
            edo ln -s /usr/$(exhost --target)/bin ./
            edo popd
            edo env GOARCH=${go_arch} GOHOSTARCH=${go_host_arch} GOROOT_BOOTSTRAP="${WORK}/bootstrap" ./make.bash
        fi
    fi
}

go_src_test() {
    cd src
    edo env GOARCH=${go_arch} GOHOSTARCH=${go_host_arch} GOROOT="${WORKBASE}"/${PNV} PATH="${WORK}/bin:${PATH}" ./run.bash --no-rebuild
}

go_src_install() {
    edo mkdir -p "${IMAGE}"/usr/$(exhost --target)/{lib/go,bin}
    edo cp -rp api bin lib pkg src "${IMAGE}"/usr/$(exhost --target)/lib/go
    edo chown -R root:root "${IMAGE}"/usr

    for bin in "${IMAGE}"/usr/$(exhost --target)/lib/go/bin/*;do
        dosym /usr/$(exhost --target)/lib/go/bin/${bin##*/} /usr/$(exhost --target)/bin/${bin##*/}
    done

    edo mkdir -p "${IMAGE}"/usr/share/doc/${PNVR}
    dosym /usr/$(exhost --target)/lib/go/api /usr/share/doc/${PNVR}/api

    # XXX: remove testdata, which hit cave fix-linkage
    edo rm "${IMAGE}"/usr/$(exhost --target)/lib/go/src/debug/elf/testdata/gcc-386-freebsd-exec

    find "${IMAGE}"/usr/$(exhost --target)/lib/"${PN}"/ -name "*.bat" -exec rm {} \;
    edo rm -rf "${IMAGE}"/usr/$(exhost --target)/lib/go/pkg/linux_*/internal/syscall/windows
    edo rm -rf "${IMAGE}"/usr/$(exhost --target)/lib/go/pkg/obj/linux_*
    edo rm -rf "${IMAGE}"/usr/$(exhost --target)/lib/go/pkg/bootstrap
    edo rm -r "${IMAGE}"/usr/$(exhost --target)/lib/go/pkg/obj

    emagicdocs
}

