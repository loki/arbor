# Copyright 2019 Marc-Antoine Perennou <keruspe@exherbo.org>
# Copyright 2018 Bjorn Pagen <bjornpagen@gmail.com>
# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Copyright 2015 Johannes Nixdorf <mixi@exherbo.org>
# Copyright 2012 Elias Pipping <pipping@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require llvm-project [ projects=[ "${PN}" "polly" ] slotted=true asserts=true ]
require alternatives

export_exlib_phases src_unpack src_prepare src_configure src_compile src_test src_test_expensive src_install

SUMMARY="The LLVM Compiler Infrastructure"

MYOPTIONS+="
    libedit
    polly [[ description = [ High-Level Loop and Data-Locality Optimizations ] ]]
"

# FIXME: need a way to specify "when cross-compiling need llvm for build host" dependency
DEPENDENCIES+="
    build:
        dev-lang/perl:*
        dev-python/setuptools[python_abis:*(-)?]
        sys-devel/flex
    build+run:
        dev-libs/libxml2:2.0
        sys-libs/zlib
        libedit? ( dev-libs/libedit )
        !sys-devel/binutils[=2.30] [[
            description = [ gold regressed in 2.30 ]
            resolution = upgrade-blocked-before
        ]]
    test:
        dev-python/psutil[python_abis:*(-)?]
    run:
        !dev-lang/llvm:0[<5.0.1-r1] [[
            description = [ Old, unslotted llvm not supported ]
            resolution = upgrade-blocked-before
        ]]
"
# FIXME: fails to find some missing headers?
#        polly? ( dev-libs/isl:=[>=0.20] )

DEFAULT_SRC_PREPARE_PATCHES+=(
    "${FILES}"/0001-Allow-CMAKE_BUILD_TYPE-None.patch
)

CMAKE_SRC_CONFIGURE_PARAMS+=(
    # TODO(compnerd) hidden inline visibility causes test tools to fail to build as a required
    # method is hidden; move the definition out of line, and export the interface
    -DSUPPORTS_FVISIBILITY_INLINES_HIDDEN_FLAG:BOOL=OFF

    -DLLVM_BINUTILS_INCDIR:STRING=/usr/$(exhost --is-native -q || echo "$(exhost --build)/$(exhost --build)/")$(exhost --target)/include/
    -DLLVM_DEFAULT_TARGET_TRIPLE:STRING=$(exhost --target)
    -DLLVM_INCLUDE_TESTS:BOOL=TRUE

    # We always want to build LLVM's dylib, which is a shared library
    # that basically contains all of the split libraries. It's required
    # for most stuff that wants to link against LLVM. We can still build
    # LLVM statically by setting BUILD_SHARED_LIBS to FALSE below.
    -DLLVM_BUILD_LLVM_DYLIB:BOOL=TRUE

    # install LLVM to a slotted directory to prevent collisions with other llvm's
    -DCMAKE_INSTALL_PREFIX:STRING=${LLVM_PREFIX}
    -DCMAKE_INSTALL_MANDIR:STRING=${LLVM_PREFIX}/share/man

    # install utils (FileCheck, count, not) to `llvm-config --bindir`, so that
    # clang and others can use them
    -DLLVM_INSTALL_UTILS:BOOL=TRUE

    # needs perfmon2
    -DLLVM_ENABLE_LIBPFM:BOOL=FALSE

    -DLLVM_ENABLE_LIBXML2:BOOL=TRUE
    -DLLVM_ENABLE_WERROR:BOOL=OFF
)

CMAKE_SRC_CONFIGURE_OPTIONS+=(
    'libedit LLVM_ENABLE_LIBEDIT'
    'polly LLVM_POLLY_BUILD'
    'polly LLVM_POLLY_LINK_INTO_TOOLS'
)

CMAKE_SRC_CONFIGURE_TESTS=(
    # Build tests in src_compile instead of src_test
    '-DLLVM_BUILD_TESTS:BOOL=ON -DLLVM_BUILD_TESTS:BOOL=OFF'
    '--expensive -DLLVM_ENABLE_EXPENSIVE_CHECKS:BOOL=ON -DLLVM_ENABLE_EXPENSIVE_CHECKS:BOOL=OFF'
)

llvm_src_unpack() {
    llvm-project_src_unpack

    edo ln -s "${WORKBASE}"/llvm-project/polly "${CMAKE_SOURCE}"/tools/polly
}

llvm_src_prepare() {
    cmake_src_prepare

    # Fix the use of dot
    edo sed -e 's/@DOT@//g' -i docs/doxygen.cfg.in

    # These tests fail if gcc is not in path
    edo sed -e "s/bugpoint/\0 --gcc=${CC}/" -i test/BugPoint/*.ll

    # TODO(marvs): Need to investigate why these tests fail
    #
    # Exit Code: 1
    # Command Output (stdout):
    # --
    # Read input file      : '/var/tmp/paludis/build/dev-lang-llvm-8.0.0rc2/work/llvm-8.0.0rc2.src/test/BugPoint/func-attrs.ll'
    # *** All input ok
    # Running selected passes on program to test for crash: Success!
    # Initializing execution environment: Found lli: /var/tmp/paludis/build/dev-lang-llvm-8.0.0rc2/work/build/bin/lli
    # Sorry, I can't automatically select a safe interpreter!
    #
    # Exiting.
    edo rm test/BugPoint/func-attrs-keyval.ll
    edo rm test/BugPoint/func-attrs.ll

    # Hangs
    edo rm -r test/tools/opt-viewer/

    # Python 3.8 compat
    [[ $(python_get_abi) == 2* ]] || edo sed -e 's/cgi/html/' -i tools/opt-viewer/opt-viewer.py
}

llvm_src_configure() {
    local args=()

    if ! exhost --is-native -q; then
        args+=(
            -DLLVM_TABLEGEN:STRING="/usr/$(exhost --build)/lib/llvm/${SLOT}/bin/llvm-tblgen"
        )
    fi

    if option polly; then
        args+=(
            # FIXME: fails to build with our system isl
            -DPOLLY_BUNDLED_ISL:BOOL=ON
            -DPOLLY_ENABLE_GPGPU_CODEGEN:BOOL=OFF
        )
    fi

    cmake_src_configure "${args[@]}"
}

llvm_src_compile() {
    ninja_src_compile

    edo pushd "${CMAKE_SOURCE}"/utils/lit
    setup-py_src_compile
    edo popd
}

llvm_src_test() {
    # TODO(Cogitri): As far as I can see LLVM doesn't offer a seperate
    # command to run non-expensive or expensive tests, so I resorted
    # to the following:
    expecting_tests --expensive || llvm-project_src_test
}

llvm_src_test_expensive() {
    llvm-project_src_test
}

llvm_src_install() {
    cmake_src_install

    # Make sure e.g. clang will not look for tools in the build directory
    edo sed \
        -e 's:^set(LLVM_TOOLS_BINARY_DIR .*)$:set(LLVM_TOOLS_BINARY_DIR '${LLVM_PREFIX}'/bin):' \
        -i "${IMAGE}${LLVM_PREFIX}"/lib/cmake/llvm/LLVMConfig.cmake

    # Symlink dynlibs to /usr/lib
    edo pushd "${IMAGE}${LLVM_PREFIX}/lib"
    for lib in $(ls libLLVM-*.so); do
        dosym "${LLVM_PREFIX}/lib/${lib}" "/usr/$(exhost --target)/lib/${lib}"
    done
    edo popd

    # Remove empty directory
    if ! ever at_least "9.0.1_rc1" ; then
        edo rmdir "${IMAGE}${LLVM_PREFIX}"/include/llvm/MC/MCAnalysis
    fi

    nonfatal edo rmdir "${IMAGE}${LLVM_PREFIX}"/include/llvm/TextAPI/MachO
    nonfatal edo rmdir "${IMAGE}${LLVM_PREFIX}"/include/llvm/BinaryFormat/WasmRelocs

    option polly && ! ever at_least "9.0.1_rc1" && edo rmdir "${IMAGE}${LLVM_PREFIX}"/include/polly/isl/deprecated

    # Manage alternatives for llvm binaries and manpages
    alternatives=()

    edo pushd "${IMAGE}${LLVM_PREFIX}/bin"
    for bin in $(ls); do
        alternatives+=("/usr/$(exhost --target)/bin/${bin}" "${LLVM_PREFIX}/bin/${bin}")
    done
    edo popd

    # allows using the non-llvm-prefixed ar, nm, ranlib on lto objects
    alternatives+=( /usr/$(exhost --build)/$(exhost --target)/lib/bfd-plugins/LLVMgold.so
                    "${LLVM_PREFIX/$(exhost --target)/$(exhost --build)}"/lib/LLVMgold.so )

    ALTERNATIVES_llvm_DESCRIPTION="Alternatives for LLVM"
    alternatives_for "llvm" "${SLOT}" "${SLOT}" "${alternatives[@]}"
}

