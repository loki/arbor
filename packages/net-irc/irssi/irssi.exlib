# Copyright (c) 2007 Alexander Færøy <ahf@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ release=${PV} suffix=tar.gz ]

export_exlib_phases src_install

SUMMARY="Irssi is a terminal based IRC client for UNIX systems"
HOMEPAGE="https://www.irssi.org/"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="perl proxy socks
    bot [[ description = [ Run an irssi module, such as a bot, without a UI ] ]]
    otr [[ description = [ Enable OTR support ] ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/glib:2[>=2.28.0]
        sys-libs/ncurses
        otr? (
            dev-libs/libgcrypt[>=1.2.0]
            net-libs/libotr[>=4.1.0]
        )
        perl? ( dev-lang/perl:=[>=5.6] )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
        socks? ( net-proxy/dante )
"

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( bot perl otr proxy socks )
DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-true-color
    --with-perl-lib=vendor
)
DEFAULT_SRC_INSTALL_PARAMS=( docdir=/usr/share/doc/${PNVR} )

irssi_src_install() {
    default
    if option perl; then
        local perllocal=( "${IMAGE}"/usr/$(exhost --target)/lib/perl5/*/*/perllocal.pod )
        edo rm "${perllocal[0]}"
        edo rmdir "${perllocal[0]%/*}"
        edo rmdir "${perllocal[0]%/*/*}"
    fi
}

