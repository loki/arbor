# Copyright 2009-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

MY_PV=${PV/_p/-P}
MY_PNV=${PN//-tools}-${MY_PV}

export_exlib_phases src_prepare src_compile src_install

SUMMARY="Tools from the bind package: dig, dnssec-keygen, host, nslookup, nsupdate"
DESCRIPTION="
DNS tools like dig and nslookup are extremely useful to have. They usually ship
with bind itself but it doesn't make any sense to install the full bind package
for them. Thus, this allows you to install just the tools.
"
HOMEPAGE="https://www.isc.org/software/bind"
DOWNLOADS="https://ftp.isc.org/isc/bind${PV%%.*}/${MY_PV}/${MY_PNV}.tar.gz"

BUGS_TO="philantrop@exherbo.org"

LICENCES="MPL-2.0"
SLOT="0"
MYOPTIONS="
    geoip [[ description = [ ACLs can also be used for geographic access restrictions. ] ]]
    idn
    kerberos
    kerberos? ( ( providers: heimdal krb5 ) [[ number-selected = exactly-one ]] )
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

# Tons of sandbox violations.
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/libxml2:2.0
        sys-libs/readline:=
        geoip? ( net-libs/libmaxminddb )
        idn? ( net-dns/libidn2:= )
        kerberos? (
            providers:heimdal? ( app-crypt/heimdal )
            providers:krb5? ( app-crypt/krb5 )
        )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl[>=1.0.0] )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-dnstap
    --disable-fips-mode
    --disable-static
    --with-libxml2
    --with-openssl=/usr/$(exhost --target)
    --with-readline=-lreadline
    --with-zlib
    --without-cmocka
    --without-geoip
    --without-libjson
    --without-lmdb
    --without-python
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'geoip geoip2'
    'idn libidn2'
    'kerberos gssapi'
)

WORK=${WORKBASE}/${MY_PNV}

bind-tools_src_prepare() {
    # TODO: Report upstream
    edo sed -i "/AC_PATH_PROG(AR/d" configure.ac
    edo sed -i 's/"nm"/"'$(exhost --tool-prefix)'nm"/' util/mksymtbl.pl

    autotools_src_prepare
}

bind-tools_src_compile() {
    for subdir in lib bin/delv bin/dig bin/nsupdate bin/dnssec ; do
        edo cd "${WORK}"/${subdir}
        emake -j1
    done
}

bind-tools_src_install() {
    dobin bin/delv/delv
    dobin bin/dig/{dig,host,nslookup}
    dobin bin/dnssec/dnssec-keygen
    dobin bin/nsupdate/nsupdate

    doman bin/delv/delv.1
    doman bin/dig/{dig.1,host.1,nslookup.1}
    doman bin/dnssec/dnssec-keygen.8
    doman bin/nsupdate/nsupdate.1
}

