# Copyright 2013-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require bash-completion

SUMMARY="ipset is used to maintain IP sets in the kernel"
DESCRIPTION="
ipset allows administration of sets of IP addresses/networks, ports, MAC addresses,
and interfaces, which are stored in hash or bitmap data structures. These can then
be used in conjunction with iptables to do fast presence lookups.
"
HOMEPAGE="http://ipset.netfilter.org"
DOWNLOADS="${HOMEPAGE}/${PNV}.tar.bz2"

BUGS_TO="philantrop@exherbo.org"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
    build+run:
        net-libs/libmnl[>=1]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-bashcompl
    --disable-static
    --without-kmod
)

src_install() {
    default

    dobashcompletion utils/ipset_bash_completion/${PN}
}

