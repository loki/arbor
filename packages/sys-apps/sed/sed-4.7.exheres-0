# Copyright 2007 Bryan Østergaard
# Copyright 2009, 2010 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

require alternatives
require gnu [ suffix=tar.xz ]

SUMMARY="GNU sed stream editor"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="
    acl
    selinux

    ( parts: binaries data documentation )
    ( linguas: af ast bg ca cs da de el eo es et eu fi fr ga gl he hr hu id it
               ja ko nb nl pl pt_BR pt ro ru sk sl sr sv tr uk vi zh_CN zh_TW )
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19.2]
    build+run:
        acl? ( sys-apps/acl )
        selinux? ( security/libselinux )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-nls
    --program-prefix=g
    gt_cv_func_gnugettext{1,2}_libc=yes
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( acl )
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( selinux )

src_configure() {
    # The purpose of this test in m4/acl.m4 is to check if 'acl_get_file' works
    # and doesn't have a particular bug on Darwin 8.7.0. Since the test fails
    # when cross-compiling we assume we are not on Darwin 8.7.0 for the time being.
    exhost --is-native -q || export gl_cv_func_working_acl_get_file=yes

    default
}

src_install() {
    default

    nonfatal edo rm -rf "${IMAGE}"/usr/$(exhost --target)/lib/charset.alias
    nonfatal edo rmdir "${IMAGE}"/usr/$(exhost --target)/lib

    option parts:binaries && alternatives_for sed gnu 1000 /usr/$(exhost --target)/bin/sed gsed
    option parts:documentation && alternatives_for sed gnu 1000 /usr/share/man/man1/sed.1 gsed.1

    expart binaries /usr/$(exhost --target)/bin
    expart data /usr/share
    expart documentation /usr/share/{doc,info,man}
}

src_test() {
    esandbox allow_net --bind "inet:127.0.0.1@80"
    esandbox allow_net --connect "inet:127.0.0.1@80"

    default

    esandbox disallow_net --connect "inet:127.0.0.1@80"
    esandbox disallow_net --bind "inet:127.0.0.1@80"
}

pkg_preinst() {
    if exhost --is-native -q && [[ $(readlink -f "${ROOT##/}"/bin) == ${ROOT##/}/bin ]] ; then
        # NOTE(compnerd) preserve legacy paths for eclectic
        nonfatal edo rm "${ROOT}"/bin/sed &&
        nonfatal edo cp "${IMAGE}"/usr/$(exhost --build)/bin/sed "${ROOT}"/bin/sed ||
            eerror "Copying /bin/sed failed"
    fi
}

