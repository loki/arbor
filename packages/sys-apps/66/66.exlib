# Copyright 2018-2019 Danilo Spinella <danyspin97@protonmail.com>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon s6-linux-init.exlib, which is:
#   Copyright 2016-2019 Johannes Nixdorf <mixi@exherbo.org>
#   Distributed under the terms of the GNU General Public License v2

require alternatives
require gitlab [ prefix=https://framagit.org user=obarun tag=v${PV} ]

export_exlib_phases src_compile src_install

SUMMARY="Helper tools around s6 supervision suite"
HOMEPAGE="https://framagit.org/Obarun/${PN}"

LICENCES="ISC"
SLOT="0"
MYOPTIONS="
    man-pages [[ description = [ Generate and install man pages ] ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        man-pages? ( app-doc/scdoc[>=1.9.4] )
    build+run:
        dev-lang/execline[>=2.5.1.0]
        dev-libs/oblibs[>=0.0.1.3]
        dev-libs/skalibs[>=2.8.1.0]
        sys-apps/s6[>=2.8.0.1]
        sys-apps/s6-rc[>=0.5.0.0]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --with-system-dir=/var/lib/${PN}
    --with-skeleton=/usr/$(exhost --target)/libexec/66
    --enable-shared
    --disable-allstatic
)

DEFAULT_SRC_COMPILE_PARAMS=(
    AR=$(exhost --tool-prefix)ar
    RANLIB=$(exhost --tool-prefix)ranlib
)

66_src_compile() {
    default

    if option man-pages ; then
        emake man
    fi
}

66_src_install() {
    default

    if option man-pages ; then
        emake DESTDIR="${IMAGE}" install-man
    fi

    keepdir /etc/66/service
    keepdir /etc/66/conf
    keepdir /usr/share/66/service
    keepdir /var/lib/66
    keepdir /var/log/66

    local a alternatives=()

    for a in init halt poweroff reboot shutdown; do
        alternatives+=(
            /usr/$(exhost --target)/bin/${a} \
                    /usr/$(exhost --target)/libexec/66/${a}
        )
    done

    alternatives_for init ${PN} 500 "${alternatives[@]}"
}

