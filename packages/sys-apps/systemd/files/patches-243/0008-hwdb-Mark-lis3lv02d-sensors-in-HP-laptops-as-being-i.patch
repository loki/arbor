Source/Upstream: Yes, fixed in v243-stable branch and master

From 4247938ee13e23eae1afcecbe646de5283b7afc2 Mon Sep 17 00:00:00 2001
From: Hans de Goede <hdegoede@redhat.com>
Date: Thu, 5 Sep 2019 14:16:12 +0200
Subject: [PATCH 08/51] hwdb: Mark lis3lv02d sensors in HP laptops as being in
 the base

The lis3lv02d sensor used in many HP laptops is (almost) always intented
primarily for freefall detection / HDD protection and (almost) always
is located in the base of a classic clamshell laptop

Before we had the ACCEL_LOCATION udev property the issues this caused
with screen-rotation were fixed by applying a mount-matrix which
translates base-coordinates to display-coordinates assuming the display
is at an angle of exact 90 degrees to the base (swap Y and Z axis).

The comment calls this translate "from "can play neverball" to
"matches Windows 8 orientation"" but what it really does is translate
base accel-axis to display accel-axis. Thus allows rotating the screen
if you put the laptop on its side, but no-one normally does that with
a 2Kg clamshell laptop.

The obviously correct thing to do on classic clamshell laptops (not 2-in-1s)
is to disable automatic screen rotation. This commit marks the accelerometer
in these laptops as being part of the base, which will make iio-sensor-proxy
disable automatic screen rotation.

This commit also removes the orientation-matrix since the unmodified coordinates
coming from the sensor are oriented correctly for a sensor in the base.

Also see the "Bad accelerometer values cause incorrect screen rotation"
systemd-devel mail-thread from September 2019.
---
 hwdb/60-sensor.hwdb | 17 ++---------------
 1 file changed, 2 insertions(+), 15 deletions(-)

diff --git a/hwdb/60-sensor.hwdb b/hwdb/60-sensor.hwdb
index 72989a7750..1fe3c57adb 100644
--- a/hwdb/60-sensor.hwdb
+++ b/hwdb/60-sensor.hwdb
@@ -282,22 +282,9 @@ sensor:modalias:acpi:KIOX000A*:dmi:bvnAmericanMegatrendsInc.:bvr5.11:bd05/25/201
 # HP
 #########################################
 
-# Laptops using the lis3lv02d device should have a first quirk applied
-# to them in the drivers/platform/x86/hp_accel.c in the kernel. The
-# quirk from "can play neverball" to "matches Windows 8 orientation"
-# is then applied below.
+# Most HP Laptop using the lis3lv02d device have it in the base,
+# mark these sensors as such.
 sensor:modalias:platform:lis3lv02d:dmi:*svn*Hewlett-Packard*:*
- ACCEL_MOUNT_MATRIX=1, 0, 0; 0, 0, -1; 0, 1, 0
-
-# HP laptops which have the lis3lv02d device in the base, tell iio-sensor-proxy
-# about this so that the sensor is not used for display orientation
-sensor:modalias:platform:lis3lv02d:dmi:*svn*Hewlett-Packard*:*pnHPProBook4535s*
- ACCEL_LOCATION=base
-
-sensor:modalias:platform:lis3lv02d:dmi:*:svnHewlett-Packard:pnHPENVY17NotebookPC:*
- ACCEL_LOCATION=base
-
-sensor:modalias:platform:lis3lv02d:dmi:*svnHP:pnHPEliteBook850G3*
  ACCEL_LOCATION=base
 
 sensor:modalias:acpi:SMO8500*:dmi:*:svnHewlett-Packard:pnHPStream7Tablet:*
-- 
2.23.0

