# Copyright 2019 Marc-Antoine Perennou <keruspe@exherbo.org>
# Copyright 2011 Elias Pipping <pipping@exherbo.org>
# Copyright 2017 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require llvm-project [ projects=[ "libcxx" ] check_target="check-libcxx" asserts=true rtlib=true ]

export_exlib_phases src_test

SUMMARY="A new implementation of the C++ standard library"

MYOPTIONS+="
    ( libc: musl )
"

DEPENDENCIES="
    build+run:
        sys-libs/libc++abi[providers:compiler-rt?][providers:libgcc?]
"

if ever at_least 9.0.0rc1; then
    DEFAULT_SRC_PREPARE_PATCHES=(
        "${FILES}"/libc++-9-Fix-make_shared-for-types-with-noexcept-false-dtor.patch
        -p0 "${FILES}"/linux_distribution-hack.patch
    )
fi

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DLIBCXX_CXX_ABI:STRING="libcxxabi"
    -DLIBCXX_CXX_ABI_INCLUDE_PATHS:PATH="/usr/$(exhost --target)/include/libc++abi"
    -DLIBCXX_ENABLE_EXCEPTIONS:BOOL=ON
    -DLIBCXX_ENABLE_RTTI:BOOL=ON
    -DLIBCXX_ENABLE_SHARED:BOOL=ON
    -DLIBCXX_ENABLE_STATIC:BOOL=ON
    -DLIBCXX_ENABLE_STATIC_ABI_LIBRARY:BOOL=OFF
    -DLIBCXX_ENABLE_THREADS:BOOL=ON
    -DLIBCXX_ENABLE_WERROR:BOOL=OFF
    -DLIBCXX_INCLUDE_TESTS:BOOL=ON
    -DLIBCXX_INSTALL_HEADERS:BOOL=ON
    -DLIBCXX_INSTALL_LIBRARY:BOOL=ON
    -DLIBCXX_INSTALL_SHARED_LIBRARY:BOOL=ON
    -DLIBCXX_INSTALL_STATIC_LIBRARY:BOOL=ON
    -DLIBCXX_STATICALLY_LINK_ABI_IN_STATIC_LIBRARY:BOOL=ON
)

CMAKE_SRC_CONFIGURE_OPTIONS+=(
    'asserts LIBCXX_ENABLE_ASSERTIONS'
    'libc:musl LIBCXX_HAS_MUSL_LIBC'
    'providers:compiler-rt LIBCXX_USE_COMPILER_RT'
    'providers:compiler-rt LIBCXXABI_USE_LLVM_UNWINDER'
)

libc++_src_test() {
    esandbox allow_net "unix:${WORK}/test/filesystem/Output/dynamic_env/test.*/socket"

    llvm-project_src_test

    esandbox disallow_net "unix:${WORK}/test/filesystem/Output/dynamic_env/test.*/socket"
}

